import math

from pythonScripts import FilterUtils


# H Filter, M,N - Image shape
def butterworth_lowpass_filter(image, d_0):
    fourier_center_shifted, M, N, H, center1, center2 = FilterUtils.filter_template(image)
    t1 = 1
    for i in range(1, M):
        for j in range(1, N):
            r1 = (i - center1) ** 2 + (j - center2) ** 2
            r = math.sqrt(r1)
            if r > d_0:
                H[i, j] = 1 / (1 + (r / d_0) ** t1)
    result = FilterUtils.get_image_in_spatial_domain(H, fourier_center_shifted)
    return result
