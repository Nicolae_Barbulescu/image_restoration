import cv2
from matplotlib import pyplot as plt


def hist_side_by_side(img, img_res, label1=None, label2=None):
    hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    hist_result = cv2.calcHist([img_res], [0], None, [256], [0, 256])

    plt.plot(hist, label=label1)
    plt.plot(hist_result, label=label2)
    plt.legend(loc='upper left')
    plt.show()


def plot_side_by_side(img, img_res, label1=None, label2=None):
    fig, axes = plt.subplots(1, 2, figsize=(11, 11))
    plt.gray()
    axes[0].imshow(img), axes[0].axis('off'), axes[0].set_title(label1, size=20)
    axes[1].imshow(img_res), axes[1].axis('off'), axes[1].set_title(label2, size=20)
    plt.show()

