import numpy as np


def salt(image, prob=.3):
    noisy_image = image.copy()

    if len(noisy_image.shape) == 2:
        white = 255
    else:
        color_space = noisy_image.shape[2]
        if color_space == 3:
            white = np.array([255, 255, 255], dtype='uint8')
        else:
            white = np.array([255, 255, 255, 255], dtype='uint8')
    probs = np.random.random(noisy_image.shape[:2])
    noisy_image[probs > 1 - (prob / 2)] = white
    return noisy_image
