import numpy as np


def salt_and_pepper(image, prob=.3):
    noisy_image = image.copy()

    if len(noisy_image.shape) == 2:
        black = 0
        white = 255
    else:
        color_space = noisy_image.shape[2]
        if color_space == 3:
            black = np.array([0, 0, 0], dtype='uint8')
            white = np.array([255, 255, 255], dtype='uint8')
        else:
            black = np.array([0, 0, 255], dtype='uint8')
            white = np.array([255, 255, 255, 255], dtype='uint8')
    probs = np.random.random(noisy_image.shape[:2])
    noisy_image[probs < (prob / 2)] = black
    noisy_image[probs > 1 - (prob / 2)] = white
    return noisy_image
