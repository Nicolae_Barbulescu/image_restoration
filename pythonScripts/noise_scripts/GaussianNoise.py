import numpy as np


def add_gaussian_noise(image):
    noisy_image = image.copy()
    noisy_image = noisy_image / 255.0
    noise = np.random.normal(loc=0, scale=1, size=noisy_image.shape)
    noisy = np.clip((noisy_image + noise * 0.4), 0, 1)
    noisy = np.uint8(noisy * 255)
    return noisy
