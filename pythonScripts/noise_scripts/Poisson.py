import cv2
import numpy as np


def add_poisson(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    noise = np.random.poisson(lam=int(np.mean(25 * image)), size=image.shape) - np.mean(image)
    image = image + noise
    image = image.astype(np.uint8)
    return image
