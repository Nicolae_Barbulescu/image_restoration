import numpy as np
from skimage import util


def add_speckle(image):
    image = util.random_noise(image, mode="speckle")
    image = image * 255
    image = image.astype(np.uint8)
    return image
