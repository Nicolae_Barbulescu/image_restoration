import cv2


def apply_median_blur(img):
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    result = cv2.medianBlur(img, 7)
    return result
