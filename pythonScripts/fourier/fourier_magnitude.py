import cv2 as cv
import numpy as np

img = cv.imread('../images/lion.jpg', 0)
img_fourier = np.fft.fft2(img)
fourier_shifted = np.fft.fftshift(img_fourier)
magnitude = 20 * np.log(np.abs(fourier_shifted))
cv.imwrite('lion_magnitudine.jpg', magnitude)
