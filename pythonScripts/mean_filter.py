import cv2
import numpy as np
import scipy.ndimage


def apply_mean_filter(img, k):
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    k = np.ones((k, k)) / k ** 2
    img_convolved = scipy.ndimage.filters.convolve(img, k)
    return img_convolved
