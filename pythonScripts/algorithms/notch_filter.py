import cv2
import numpy as np
from matplotlib import pyplot as plt

fig, axes = plt.subplots(2, 2, figsize=(11, 11))
plt.gray()
image = np.mean(cv2.imread("../images/cacctus.jpg"), axis=2) / 255

image = image.astype(np.float64)
print(image.shape)
axes[0, 0].imshow(image), axes[0, 0].set_title('Imaginea originală', size=20)
F1 = np.fft.fft2(image.astype(np.float64))

Fourier_image = np.fft.fftshift(F1)

for n in range(image.shape[1]):
    image[:, n] += np.cos(0.1 * np.pi * n)

axes[0, 1].imshow((20 * np.log10(0.1 + Fourier_image)).astype(int)), axes[0, 1].set_title(
    'Imaginea originala in dom.frecv', size=20)

F1 = np.fft.fft2(image)
F2 = np.fft.fftshift(F1)

axes[1, 0].imshow(image), axes[1, 0].set_title('Imaginea cu zgomot periodic', size=20)

axes[1, 1].imshow((20 * np.log10(0.1 + F2)).astype(int)), axes[1, 1].set_title(
    'Imaginea cu zgomot periodic in dom.frecv.', size=20)
F2[194:198, :233] = F2[194:198, 242:] = 0

result = abs(np.fft.ifft2(F2))

result = result * 255

plt.show()
