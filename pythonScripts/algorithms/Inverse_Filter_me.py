import cv2
import numpy as np
from matplotlib import pyplot as plt

import pythonScripts.noise_scripts.salt
from pythonScripts.histogram import histogram


def inverse_filter(image):
    ksize = 31
    kernel = np.ones((ksize, ksize)) / 31

    h, w = image.shape
    kernel_image = np.zeros((h, w))
    kernel_image[:ksize, :ksize] = kernel

    image_freq = np.fft.fft2(image)
    k_freq = np.fft.fft2(kernel_image)
    image_blur = image_freq * k_freq
    image_blur = np.fft.ifft2(image_blur).real
    epsilon = 10 ** -6

    freq = np.fft.fft2(image_blur)
    freq_kernel = k_freq
    freq_kernel = 1 / (epsilon + freq_kernel)  # epsilon folosit pentru a evita impartirea la 0
    convolved_restored = freq_kernel * freq
    im_restored = np.fft.ifft2(convolved_restored).real
    im_restored = 255 * im_restored / np.max(im_restored)

    fig, axes = plt.subplots(2, 2, figsize=(11, 11))
    plt.gray()
    axes[0, 0].imshow(image), axes[0, 0].axis('off'), axes[0, 0].set_title('Imaginea originală', size=20)
    axes[0, 1].imshow(image_blur), axes[0, 1].axis('off'), axes[0, 1].set_title('Imaginea blurată', size=20)
    axes[1, 0].imshow(im_restored), axes[1, 0].axis('off'), axes[1, 0].set_title('Imaginea restaurată', size=20)
    axes[1, 1].imshow(im_restored - image), axes[1, 1].axis('off'), axes[1, 1].set_title(
        ' Imaginea restaurată - imaginea originală', size=20)
    # show in interface
    plt.show()
    return im_restored


image = cv2.imread('../images/lion.jpg', 0)
image = pythonScripts.noise_scripts.salt.salt(image)
result = inverse_filter(image)
result = result.astype(np.uint8)
histogram.hist_side_by_side(image, result, "Imaginea cu zgomot", "Imaginea dupa restaurare")
