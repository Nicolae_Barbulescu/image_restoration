import cv2
import numpy
import scipy.fftpack as fftim
from PIL import Image


# H Filter, M,N - Image shape
def filter_template(image):
    if len(image.shape) == 3:
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    image_fourier = fftim.fft2(image)
    fourier_center_shifted = fftim.fftshift(image_fourier)
    M = fourier_center_shifted.shape[0]
    N = fourier_center_shifted.shape[1]
    H = numpy.ones((M, N))
    center1 = M / 2
    center2 = N / 2
    return fourier_center_shifted, M, N, H, center1, center2


def get_image_in_spatial_domain(H, fourier_center_shifted):
    H = Image.fromarray(H)
    con = fourier_center_shifted * H
    image_spatial_domain = abs(fftim.ifft2(con))
    return image_spatial_domain
