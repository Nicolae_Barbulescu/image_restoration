import cv2
import scipy.ndimage


# Exemplu: pentru Kernel de 3x3
# 11, 5, 22, 4, 16, 74, 255, 3, 133
# Valorile sunt sortate:
# 3, 4, 5, 11, 16, 22, 74, 133, 255
# Se alege valoarea care imparte lista in 2 parti cu un numar egal de elemente
# Valoarea 16 este noua valoarea a pixelului.

def apply_median_filter(img, k):
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    image_convolved = scipy.ndimage.filters.median_filter(img, k)
    return image_convolved
