import os
import tkinter as tk
from tkinter import filedialog
from tkinter import ttk

from PIL import ImageTk


class UploadImage(tk.Frame):
    def __init__(self, container, **kwargs):
        super().__init__(container, **kwargs)

        self.image_path = None
        self['bg'] = 'grey13'
        # label
        self.label = ttk.Label(self, text="Incarca o imagine", justify="right", style='TLabel')
        self.label.grid(row=0, column=0, sticky="NSE")
        # upload button logo
        self.upload_logo = ImageTk.PhotoImage(file="logo_images/upload_white.png")
        self.upload = ttk.Button(self, text="Upload", image=self.upload_logo, command=self.browseFiles, style='TLabel')
        self.upload.grid(row=0, column=1, sticky="NSW")

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)
        # image name
        self.image_name = ttk.Label(self, style='TLabel', justify="center")
        self.image_name.grid(row=1, column=0, sticky="NSE", padx=60)

    def browseFiles(self):
        filename = filedialog.askopenfilename(initialdir="/",
                                              title="Select a File",
                                              filetypes=[
                                                  ("JPEG,PNG", ".jpg .png"),
                                                  ('All files', "*")])
        if filename:
            self.image_name['text'] = os.path.basename(filename)
            self.image_path = filename
