import tkinter as tk
from tkinter import ttk

import cv2
from PIL import ImageTk
from PyQt5 import QtGui

from ResultPage import ResultPage
from Upload import UploadImage
from interface.PickAlg import DeconvolutionAlgorithms
from interface.PickNoise import NoiseAlgorithm
from pythonScripts import mean_filter, median_filter, Ideal_Low_Pass_Filter, Butterworth, Gaussian

WINDOW_WIDTH = 1500
WINDOW_HEIGHT = 1000
NUMBER_OF_FRAMES = 3


def center(window):
    # get desktop resolution
    window.update_idletasks()
    app = QtGui.QGuiApplication([])
    screen_width = app.primaryScreen().geometry().width()
    screen_height = app.primaryScreen().geometry().height()

    size = tuple(int(_) for _ in window.geometry().split('+')[0].split('x'))
    x = screen_width / 2 - size[0] / 2
    y = screen_height / 2 - size[1] / 2
    window.geometry("+%d+%d" % (x, y))
    return x, y


class HomePage(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        height_min = WINDOW_HEIGHT
        width_min = WINDOW_WIDTH // 2
        self.minsize(width=width_min, height=height_min)
        self.geometry("{}x{}".format(WINDOW_WIDTH, WINDOW_HEIGHT))
        self.title("Upload image")
        self.bind('<Escape>', lambda x: self.destroy())
        self.frame_displayed = 0
        self.result = {}

        self.alg_dict = {
            'Mean Filter': lambda image, kernel: mean_filter.apply_mean_filter(image, kernel),
            'Median Filter': lambda image, kernel: median_filter.apply_median_filter(image, kernel),
            'Ideal Lowpass Filter': lambda image, cut_off: Ideal_Low_Pass_Filter.ideal_lowpass_filter(image, cut_off),
            'Butterworth Lowpass Filter': lambda image, cut_off: Butterworth.butterworth_lowpass_filter(image, cut_off),
            'Gaussian Lowpass Filter': lambda image, cut_off: Gaussian.gaussian_lowpass_filter(image, cut_off),

        }

        self.file_path = None
        self.image_noise = None
        self.style = ttk.Style()
        self.style.configure('TLabel', foreground='white', background='grey13', font=("Helvetica", 24), relief='flat')

        center(self)

        self.template_frame = tk.Frame(self)
        self.template_frame.pack(fill='both', expand=True)
        self.canvas = tk.Canvas(self.template_frame, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
        self.canvas.pack(fill='both', expand=True)
        background_image = ImageTk.PhotoImage(file=r"logo_images/math2jpg.jpg")
        self.image = background_image
        self.canvas.create_image((0, 0), image=self.image, anchor='nw')

        title = ttk.Label(self.canvas, text='Restaurarea imaginilor', relief='flat', style='TLabel',
                          font=('Helvetica', 32))

        title.pack(side=tk.TOP, pady=10)

        # Home button
        self.home_logo = ImageTk.PhotoImage(file="logo_images/home.png")
        self.home = ttk.Button(self.canvas, text="Home", image=self.home_logo, command=self.go_home, style='TLabel')
        self.home.pack(anchor='w', padx=50, pady=5)
        # FRAMES
        self.upload_frame = UploadImage(self.canvas)
        self.upload_frame.pack(side=tk.TOP, fill='both')

        self.noise_algorithm = NoiseAlgorithm(self.canvas)
        self.deconvolution_algorithms = DeconvolutionAlgorithms(self.canvas)

        self.resultFrame = ResultPage.__class__
        # Save frames
        self.frames = dict()
        for (i, FrameClass) in enumerate([self.upload_frame, self.noise_algorithm, self.deconvolution_algorithms]):
            self.frames[i] = FrameClass

        # get selected options
        self.next_button = tk.Button(self.canvas, text="Inainte", height=2, width=15, bg='grey13', fg='white',
                                     font=('Helvetica', 14),
                                     command=self.nextButton)
        self.next_button.pack(anchor='s', side=tk.RIGHT, ipadx=15, ipady=10, padx=50, pady=100)

        self.back_button = tk.Button(self.canvas, text="Inapoi", height=2, width=15, bg='grey13', fg='white',
                                     font=('Helvetica', 14),
                                     command=self.backButton)
        self.back_button.pack(anchor='s', side=tk.LEFT, ipadx=15, ipady=10, padx=50, pady=100)

    def go_home(self):
        self.frame_displayed = 0
        self.show_frame()

    def apply_selected_alg(self, selected_algorithms, kernel, cut_off):
        self.result = {}
        for algorithm in selected_algorithms:
            # [original, with_noise, restored] as array
            original_image = cv2.imread(self.file_path)
            original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)
            if self.image_noise is not None:
                if algorithm in ['Mean Filter', 'Median Filter']:
                    img_list = [original_image, self.image_noise,
                                self.alg_dict[algorithm](image=self.image_noise, kernel=kernel)]
                else:
                    img_list = [original_image, self.image_noise,
                                self.alg_dict[algorithm](image=self.image_noise, cut_off=cut_off)]
            else:
                if algorithm in ['Mean Filter', 'Median Filter']:
                    img_list = [original_image, original_image,
                                self.alg_dict[algorithm](image=original_image, kernel=kernel)]
                else:
                    img_list = [original_image, original_image,
                                self.alg_dict[algorithm](image=original_image, cut_off=cut_off)]
            self.result[algorithm] = img_list

        self.resultFrame = ResultPage(self.canvas, self.result)
        self.frames[NUMBER_OF_FRAMES] = self.resultFrame

    def show_frame(self):
        frame = self.frames[self.frame_displayed]
        for f in self.frames.values():
            if f != frame:
                f.pack_forget()
        frame.pack(fill='both')

    def nextButton(self):
        if self.upload_frame.image_path is None:
            return
        elif self.frame_displayed == 1:
            self.image_noise = self.noise_algorithm.noisy_image

        elif self.frame_displayed == 2:
            selected_algorithms, kernel, cut_off = DeconvolutionAlgorithms.selected_algorithms(
                self.deconvolution_algorithms)
            if kernel == '':
                kernel = 5
            else:
                try:
                    kernel = int(kernel)
                except ValueError:
                    print("Nu este un numar")
            if cut_off == '':
                cut_off = 30.0
            else:
                try:
                    cut_off = float(cut_off)
                except ValueError:
                    print("Nu este un numar")
            if selected_algorithms is not None:
                self.apply_selected_alg(selected_algorithms, kernel, cut_off)

        if self.frame_displayed != NUMBER_OF_FRAMES:
            self.increase_frame_counter()

    def backButton(self):

        if self.frame_displayed != 0:
            self.decrease_frame_counter()

    def decrease_frame_counter(self):
        self.frame_displayed -= 1
        self.show_frame()

    def increase_frame_counter(self):
        self.frame_displayed += 1
        if self.frame_displayed == 1:
            self.file_path = self.upload_frame.image_path
            self.noise_algorithm.image_path = self.file_path
        self.show_frame()


if __name__ == '__main__':
    root = HomePage()
    root.mainloop()
