import tkinter as tk

import cv2
from PIL import ImageTk, Image

from pythonScripts.noise_scripts import SaltAndPepper, SpeckleNoise, GaussianNoise, Poisson, MedianBlur, salt


class NoiseAlgorithm(tk.Frame):
    def __init__(self, container, **kwargs):
        super().__init__(container, **kwargs)
        self.alg_dict = {
            'Salt and pepper': lambda image: SaltAndPepper.salt_and_pepper(image),
            'Salt': lambda image: salt.salt(image),
            'Gaussian Noise': lambda image: GaussianNoise.add_gaussian_noise(image),
            'Poisson Noise': lambda image: Poisson.add_poisson(image),
            'Median Blur': lambda image: MedianBlur.apply_median_blur(image),
            'Speckle': lambda image: SpeckleNoise.add_speckle(image)

        }
        self.image_path = None
        self['bg'] = 'grey13'
        list_of_algorithms = ('Gaussian Noise', 'Salt and pepper', 'Poisson Noise', 'Median Blur', 'Salt')
        lang_var = tk.StringVar(value=list_of_algorithms)
        self.list_box = tk.Listbox(self, listvariable=lang_var, selectmode='extended', font=('Helvetica', 24),
                                   foreground='white', background='grey13', justify='center')
        self.list_box.pack(expand=tk.YES, fill='both')

        self.dict_result = {}
        self.canvas_image = None
        self.noisy_image = None
        self.calculate = tk.Button(self, text="Calculeaza", height=2, width=15, bg='grey13', fg='white',
                                   font=('Helvetica', 14), command=self.calculate)
        self.calculate.pack(side=tk.RIGHT)

        self.reset = tk.Button(self, text="Reseteaza", height=2, width=15, bg='grey13', fg='white',
                               font=('Helvetica', 14), command=self.reset)
        self.reset.pack(side=tk.LEFT)
        self.canvas = tk.Canvas(self)

    def calculate(self):
        if self.image_path is None:
            return
        original_image = cv2.imread(self.image_path)
        selected = self.selected_algorithms()
        if selected[0] == '':
            return

        self.noisy_image = self.alg_dict[selected[0]](image=original_image)
        try:
            self.noisy_image = cv2.cvtColor(self.noisy_image, cv2.COLOR_BGR2RGB)
        except SyntaxError:
            pass
        # Apply noise
        self.canvas['width'] = self.noisy_image.shape[0]
        self.canvas['height'] = self.noisy_image.shape[1]
        self.canvas_image = ImageTk.PhotoImage(image=Image.fromarray(self.noisy_image))
        self.canvas.create_image(self.noisy_image.shape[0] // 2, self.noisy_image.shape[1] // 2,
                                 image=self.canvas_image)
        self.canvas.pack()

    def reset(self):
        if self.image_path is None:
            return
        self.noisy_image = cv2.imread(self.image_path)
        self.noisy_image = cv2.cvtColor(self.noisy_image, cv2.COLOR_BGR2RGB)
        # Apply noise
        self.canvas_image = ImageTk.PhotoImage(image=Image.fromarray(self.noisy_image))
        self.canvas.create_image(self.noisy_image.shape[0] // 2, self.noisy_image.shape[1] // 2,
                                 image=self.canvas_image)

    def selected_algorithms(self):
        selected_indices = self.list_box.curselection()

        selected_alg = ",".join([self.list_box.get(i) for i in selected_indices])
        return selected_alg.split(",")
