import tkinter as tk
from tkinter import ttk

import cv2
from PIL import Image, ImageTk


class ScrollableFrame(ttk.Frame):
    def __init__(self, container, *args, **kwargs):
        super().__init__(container, **kwargs)
        self.canvas = tk.Canvas(self)
        scrollbar = ttk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        scrollbar_x = ttk.Scrollbar(self, orient="horizontal", command=self.canvas.xview)
        self.scrollable_frame = ttk.Frame(self.canvas)

        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: self.canvas.configure(
                scrollregion=self.canvas.bbox("all")
            )
        )

        self.canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")

        self.canvas.configure(yscrollcommand=scrollbar.set)
        self.canvas.configure(xscrollcommand=scrollbar_x.set)
        self.canvas.config(width=1200, height=1200)
        self.canvas.pack(side="top", fill="both", expand=True, anchor="nw")
        scrollbar_x.pack(side=tk.BOTTOM, fill="x", anchor="s", expand=True)
        self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)

    def _on_mousewheel(self, event):
        self.canvas.yview_scroll(-1 * (event.delta // 120), "units")


class ResultPage(ScrollableFrame):

    def __init__(self, container, result, **kwargs):
        super().__init__(container, **kwargs)
        self.result = result

        labels = ['Imaginea Originala', 'Imaginea cu zgomot', 'Imaginea restaurata']
        for line, label in enumerate(labels):
            lbl = ttk.Label(self.scrollable_frame, text=label, justify="center", style='TLabel')
            lbl.grid(row=line + 1, column=0)
        if self.result is not None:
            for column, alg_choice in enumerate(self.result.keys()):
                alg_title = ttk.Label(self.scrollable_frame, text=alg_choice, justify="center", style='TLabel')
                alg_title.grid(row=0, column=(column * 2) + 1)
                for line, array in enumerate(self.result[alg_choice]):
                    if line == 1:
                        to_bgr = cv2.cvtColor(array, cv2.COLOR_RGB2GRAY)
                        cv2.imwrite("rezultate/noise_img.png", to_bgr)
                    if line == 2:
                        cv2.imwrite("rezultate/" + alg_choice + str(line) + ".png", array)

                    image = ImageTk.PhotoImage(image=Image.fromarray(array))
                    label = tk.Label(self.scrollable_frame, image=image)
                    label.image = image
                    label.grid(row=line + 1, column=(column * 2) + 1)
