import tkinter as tk
from tkinter import ttk
from tkinter.ttk import Entry


class DeconvolutionAlgorithms(tk.Frame):
    def __init__(self, container, **kwargs):
        super().__init__(container, **kwargs)

        self['bg'] = 'grey13'
        list_of_algorithms = (
            'Mean Filter', 'Median Filter', 'Ideal Lowpass Filter', 'Butterworth Lowpass Filter',
            'Gaussian Lowpass Filter')
        lang_var = tk.StringVar(value=list_of_algorithms)
        self.list_box = tk.Listbox(self, listvariable=lang_var, selectmode='extended', font=('Helvetica', 24),
                                   foreground='white', background='grey13', justify='center')
        self.list_box.grid(row=1, column=2, sticky='nsew')

        self.kernel_label = ttk.Label(self, text='Kernel', relief='flat', style='TLabel',
                                      font=('Helvetica', 24))
        self.kernel_label.grid(row=0, column=0)
        self.var_kernel_mean = tk.StringVar(self)
        self.kernel_mean = Entry(self, textvariable=self.var_kernel_mean)
        self.kernel_mean.grid(row=0, column=1)

        self.cut_off = ttk.Label(self, text='Cut-off value', relief='flat', style='TLabel',
                                 font=('Helvetica', 24))
        self.cut_off.grid(row=0, column=3)
        self.var_cut_off = tk.StringVar(self)
        self.var_cut_off_value = Entry(self, textvariable=self.var_cut_off)
        self.var_cut_off_value.grid(row=0, column=4)

        self.columnconfigure(2, weight=4)

    def selected_algorithms(self):
        selected_indices = self.list_box.curselection()

        selected_alg = ",".join([self.list_box.get(i) for i in selected_indices])
        return selected_alg.split(","), self.var_kernel_mean.get(), self.var_cut_off.get()
